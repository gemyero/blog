const mongoose = require('mongoose');

const userSchema = mongoose.Schema({

  name: String,
  age: Number,
  created_date: {
    type: Date,
    default: Date.now
  }

});

let userModel = mongoose.model('users', userSchema);

userModel.addUser = (name, age, resolve, reject) => {
  let user = new userModel({
    name,
    age
  });

  user.save()
    .then(resolve)
    .catch(reject);
}

userModel.listUsers = (resolve, reject) => {
  userModel.find({})
    .then(resolve)
    .catch(reject);
}

userModel.getUser = (id, resolve, reject) => {
  userModel.find({
      _id: id
    })
    .then(resolve)
    .catch(reject);
}

userModel.updateUser = (id, name, age, resolve, reject) => {
  userModel.update({
      _id: id
    }, {
      name: name,
      age: age
    })
    .then(resolve)
    .catch(reject);
}

userModel.removeUser = (id, resolve, reject) => {
  userModel.deleteOne({
      _id: id
    })
    .then(resolve)
    .catch(reject);
}

module.exports = userModel;
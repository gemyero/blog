const express = require('express');
const router = express.Router();
const userModel = require('../models/users');

router.route('/')

  .get((request, response, next) => {
    userModel.listUsers(
      (users) => response.json(users),
      (err) => console.log(err)
    );
  })

  .post((request, response, next) => {
    userModel.addUser(
      request.body.name,
      request.body.age,
      (user) => response.json(user),
      (err) => console.log(err)
    );
  });

router.route('/:id')

  .get((request, response, next) => {
    userModel.getUser(
      request.params.id,
      (user) => response.json(user),
      (err) => console.log(err)
    );
  })

  .delete((request, response, next) => {
    userModel.removeUser(
      request.params.id,
      () => response.json({ message: "user deleted successfully!" }),
      (err) => console.log(err)
    );
  })

  .put((request, response, next) => {
    userModel.updateUser(
      request.params.id,
      request.body.name,
      request.body.age,
      () => response.json({message: "user updated successfully!"}),
      (err) => console.log(err)
    );
  });

module.exports = router;
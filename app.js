const express = require('express');
const bodyParser = require('body-parser').urlencoded({ extended: true });
const mongoose = require('mongoose');

const usersController = require('./controllers/users');

const app = express();

// database connection
mongoose.connect('mongodb://localhost/blog');
const db = mongoose.connection;
db.on('error', () => console.log('connection error!'));
db.once('open', () => console.log('connected to database successfully!'));

// bodyParser setup
app.use(bodyParser);

// app routes 
app.use('/users', usersController);

app.listen(3000, () => console.log('Server running and listening on localhost:3000'));